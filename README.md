# Ejemplo de implementación práctica de la clase teórica de Punteros

En este ejemplo se detalla el código para realizar los pasos descriptos en la clase teórica de punteros para implementar una lista simplemente enlazada. Recuerden mirar las diapositivas de la clase teórica y el video de la clase práctica primero donde realizamos esta implementación paso a paso.

Los pasos demostrados son:

1. Declarar el `struct` usado para los nodos de la lista.
2. Cargar el alumno con libreta _1255_
3. Cargar el alumno con libreta _1322_
4. Cargar el alumno con libreta _1008_
5. Cargar el alumno con libreta _1304_
