#include <stdlib.h>
#include <stdio.h>

typedef struct nodo {
  int dato;
  struct nodo *sig;
} nodo;

int main() {
  nodo *p;

  printf("El valor de p es: %p\n", p);

  p = malloc(sizeof(nodo));
  printf("El valor de p es: %p\n", p);
  printf("El tamanio de p: %ld\n", sizeof(p));
  printf("El tamanio del elemento apuntado por p es: %ld\n", sizeof(*p));

  // Inserto el primer elemento a la lista
  p->dato = 1255;
  printf("-----------------------------------\n");
  printf("El dato de p es: %d\n", p->dato);

  // Inserto el segundo elemento a la lista
  nodo *siguiente;
  siguiente = malloc(sizeof(nodo));
  siguiente->dato = 1322;
  p->sig = siguiente;
  printf("-----------------------------------\n");
  printf("El dato de p es: %d\n", p->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->dato);

  // Inserto el tercer elemento a la lista
  siguiente = malloc(sizeof(nodo));
  siguiente->dato = 1008;
  p->sig->sig = siguiente;
  printf("-----------------------------------\n");
  printf("El dato de p es: %d\n", p->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->sig->dato);

  // Inserto el cuarto elemento a la lista
  siguiente = malloc(sizeof(nodo));
  siguiente->dato = 1304;
  p->sig->sig->sig = siguiente;
  printf("-----------------------------------\n");
  printf("El dato de p es: %d\n", p->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->sig->dato);
  printf("El dato del siguiente de p es: %d\n", p->sig->sig->sig->dato);
}
